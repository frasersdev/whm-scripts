#!/bin/sh

DOWNLOADS="post-upcp-alert.sh post-upcp-reboot.sh post-upcp-process.sh"
INSTALLPATH="/usr/local/bin"

run_install() 
{
	cd ${INSTALLPATH}

	# workaround cpanel bug CPANEL-31408
	touch /var/cpanel/hooks.yaml

	# install 
	for FILE in ${DOWNLOADS} 
	do
		wget -nv https://gitlab.com/frasersdev/whm-scripts/-/raw/master/scripts/${FILE} -O ${INSTALLPATH}/${FILE}
		chmod 755 ${INSTALLPATH}/${FILE}
	done
	
	echo
	echo
	echo "Now setup the upcp standard hook as per https://gitlab.com/frasersdev/whm-scripts/-/blob/master/README.md"
	echo
	echo
}

run_install