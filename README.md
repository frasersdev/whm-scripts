# whm-scripts
bespoke scripts for customising whm

### General Requirements

* Host can send email.
* Contact Email is configured in "Basic Webhost Manager Setup"

### Installation
Once you have examined the install script and decided it is trustworthy:
* `curl https://gitlab.com/frasersdev/whm-scripts/-/raw/master/install.sh | sh`

# post-upcp.sh
A script to inform of the reboot status and optionally reboot a cpanel server if it needs it. Designed for dnsonly servers. 
This script is configured as a standard hook into the upcp cpanel update script

### usage
Inform of reboot status only. 
* `/usr/local/cpanel/bin/manage_hooks add script /usr/local/bin/post-upcp-alert.sh  --manual --category System --event upcp --stage post`

Inform and reboot 
* `/usr/local/cpanel/bin/manage_hooks add script /usr/local/bin/post-upcp-reboot.sh --manual --category System --event upcp --stage post`

# Notes
## Standard Hooks
### To list standard hooks 
* `/usr/local/cpanel/bin/manage_hooks list`

### To add a script to upcp schema (as a hook)
* `/usr/local/cpanel/bin/manage_hooks add script /usr/local/bin/script.sh  --manual --category System --event upcp --stage post`

### To remove a script as hook from upcp schema
* `/usr/local/cpanel/bin/manage_hooks delete script /usr/local/bin/script.sh --manual --category System --event upcp --stage post`


## Bugs
cpanel / whm version 84 doesn't run standardised hooks due to a bug (CPANEL-31408) to work around the bug run:
`touch /var/cpanel/hooks.yaml`

