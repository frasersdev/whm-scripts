#!/bin/sh

#let everything settle down after upcp runs

sleep 300

#get the system email contact address
CONTACTEMAIl=`grep CONTACTEMAIL /etc/wwwacct.conf | awk '{print $2 }'`
MYHOSTNAME=`cat /etc/hostname`
THETIME=`date`

UPCPREBOOT=`/usr/local/cpanel/bin/whmapi1 system_needs_reboot | grep "needs_reboot:" | awk '{print $2 }'`

if [ "${UPCPREBOOT}" -eq 1 ]
then
        if [ "${1}" = "reboot" ]
	then
		echo "https://${MYHOSTNAME}:2087 - reboot status after upcp: ${UPCPREBOOT}. Reboot scheduled." | mailx -s "${MYHOSTNAME} is being REBOOTED. ${THETIME}" ${CONTACTEMAIl}
        	sleep 30
		/usr/sbin/reboot
	else 
		echo "https://${MYHOSTNAME}:2087 - reboot status after upcp: ${UPCPREBOOT}." | mailx -s "${MYHOSTNAME} needs a REBOOT. ${THETIME}" ${CONTACTEMAIl}
	fi
else
	echo "https://${MYHOSTNAME}:2087 - reboot status after upcp: ${UPCPREBOOT}." | mailx -s "${MYHOSTNAME} is OK. ${THETIME}" ${CONTACTEMAIl}
fi



